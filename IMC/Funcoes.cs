﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMC
{
    class Funcoes
    {
        public double CalculoIMC(Usuario Usuario)
        {


           
            
            double calculo = (Usuario.Peso) / (Math.Pow(Usuario.Altura, 2));
            return calculo;
        }

        public string Classificacao(double imc)
        {
            if(imc < 18.5)
            {
                return  "Baixo Peso";
                 
            }
            else
            {
               if(imc >= 18.5 && imc <= 24.9)
                {
                    return "Peso Normal";
                    
                }
                else
                {
                    if(imc == 25)
                    {
                        return  "Sobrepeso";
                      
                    }
                    else
                    {
                        if(imc >= 25 && imc <= 29.9)
                        {
                             return "Pré-Obeso";
                            
                        }
                        else
                        {
                            if(imc >= 30 && imc <= 34.9)
                            {
                                return "Obeso |";
                                
                            }
                            else
                            {
                                if (imc >= 35 && imc <= 39.9)
                                {
                                    return "Obeso ||";
                                    
                                }
                                else
                                {
                                    if (imc >= 40)
                                    {
                                        return "Obeso |||";
                                        
                                    }
                                }
                            }
                        }
                    }
                }

                return "Desconhecido";
                   
            }


        }


    }
}
